## 画面名
削除済み地震一覧画面

### 画面概要
論理削除済みの地震データを一覧で表示する。

### パス
/main/deleted_eq_list/【建物ID】/

### 画面レイアウト
![レイアウト](./wire_frame.png)

### 画面項目
|id|名前|種類|テーブル名|カラム名|備考|
|-|-|-|-|-|-|
|1|ヘッダー|共通|-|-|共通ヘッダー|
|2|建物情報|エリア|-|-|建物情報を表示する。|
|2-1|建物名|ラベル|building|name||
|2-2|建物画像|画像|building|image||
|2-3|建物ID|HIDDEN|building|id||
|3|検索条件|エリア|-|-|地震の検索条件項目|
|3-1|開始日|テキストボックス|-|-||
|3-2|終了日|テキストボックス|-|-||
|3-3|震度|チェックボックス|-|-|0,1,2,3,4,5弱,5強,6弱,6強,7|
|3-4|被災度|チェックボックス|-|-|無被害,軽微,小破,中破,大破|
|3-5|開始日(削除)|テキストボックス|-|-||
|3-6|終了日(削除)|テキストボックス|-|-||
|3-7|検索|ボタン|-|-||
|3-8|クリア|ボタン|-|-||
|4|削除済み地震一覧|テーブル|-|-|削除日時の降順で表示する。|
|4-1|選択|チェックボックス|-|-||
|4-2|地震発生日時|ラベル|-|-||
|4-3|震度|ラベル|-|-||
|4-4|被災度|ラベル|-|-||
|4-5|削除日時|ラベル|-|-||
|4-6|全選択|ボタン|-|-||
|4-7|全解除|ボタン|-|-||
|4-8|ページ送り|共通部品|-|-||
|4-9|地震ID|HIDDEN|-|-||
|5|復活|ボタン|-|-||
|6|完全削除|ボタン|-|-||
|7|地震管理|ボタン|-|-||

### 画面アクション
- 初期表示
    初期表示処理を実施する。
- カレンダーアイコンクリック
    カレンダーダイアログを表示する。
    カレンダーダイアログで入力した日付を開始日、終了日、開始日(削除)、終了日(削除)に設定する。
- 検索ボタンクリック
    地震検索処理を実施する。
- クリアボタンクリック
    検索条件クリア処理を実施する。
- 全選択ボタンクリック
    地震一覧の各行のチェックボックスを全てチェック状態とする。
- 全解除ボタンクリック
    地震一覧の各行のチェックボックスを全て非チェック状態とする。
- 復活ボタンクリック
    地震データ復活処理を実施する。
- 完全削除ボタンクリック
    地震データ完全削除処理を実施する。
- 地震管理ボタンクリック
    地震管理画面表示処理を実施する。

### 処理
- 初期表示処理
    1. システムは、権限チェック処理を実施し、ユーザーが画面を表示する権限を持つことを確認する。(Alt-1)
    2. システムは、建物IDを元に削除済み地震データを取得する。(Alt-2)
    3. システムは、取得した削除済み地震データを元に削除済み地震一覧を表示する。
    - Alt-1. ユーザーが画面を表示する権限を持たない場合
        1. システムは、エラー画面を表示し、ユーザーが権限を持たない旨のメッセージを表示する。
        2. ユースケースを終了する。
    - Alt-2. 削除済み地震データが0件だった場合
        1. システムは、地震データが存在しない旨のメッセージを表示する。
        2. ユースケースを終了する。
- 権限チェック処理
    1. システムは、ユーザーが統括管理権限を持つことを確認する。(Alt-1,Alt-2,Alt-3)
    2. システムは、判定の結果として権限ありを返却する。
    - Alt-1. ユーザーが企業管理権限を持つ場合
        1. システムは、建物IDと企業IDを元に建物情報が存在することを確認する。(Alt-4)
        2. システムは、判定の結果として権限ありを返却する。
    - Alt-2. ユーザーがグループ管理権限を持つ場合
        1. システムは、建物IDと企業IDを元に建物情報が存在することを確認する。(Alt-4)
        2. システムは、建物IDとグループIDを元に、建物管理権限が存在することを確認する。(Alt-5)
        3. システムは、判定の結果として権限ありを返却する。
    - Alt-3. ユーザーが一般権限を持つ場合
        1. システムは、判定の結果として権限なしを返却する。
    - Alt-4. 建物情報が存在しない場合
        1. システムは、判定の結果として権限なしを返却する。
    - Alt-5. 建物管理権限が存在しない場合
        1. システムは、判定の結果として権限なしを返却する。
- 削除済み地震検索処理
    1. システムは、開始日、終了日の値がyyyy/MM/dd形式になっていることを確認する。(Alt-1)
    2. システムは、開始日の値が終了日の値以上であることを確認する。(Alt-2)
    3. システムは、建物ID、検索条件の開始日、終了日、震度、被災度、開始日(削除)、終了日(削除)を元に
        削除済み地震データを取得する。(Alt-3)
    4. システムは、取得した削除済み地震データを元に地震一覧を表示する。
    - Alt-1. 開始日、終了日、開始日(削除)、終了日(削除)の値がyyyy/MM/dd形式でない場合
        1. システムは、検索期間の開始日・終了日、開始日(削除)・終了日(削除)は、
            「yyyy/MM/dd」の形式で入力する旨メッセージを表示する。
        2. ユースケースを終了する。
    - Alt-2. 開始日の値が、終了日の値より小さい場合
        1. システムは、検索期間は開始日より、終了日の値が大きくする旨メッセージを表示する。
        2. ユースケースを終了する。
    - Alt-3. 地震データが0件だった場合
        1. システムは、地震データが存在しない旨のメッセージを表示する。
        2. ユースケースを終了する。
- 検索条件クリア処理
    1. システムは、検索条件の開始日、終了日、開始日(削除)、終了日(削除)にブランクを設定する。
    2. システムは、検索条件の震度、被災度を全て非チェック状態とする。
- 地震データ復活処理
    1. システムは、確認ダイアログを表示し、地震データを復活させてよいか確認する旨のメッセージを表示する。
    2. ユーザーは、確認ダイアログで「はい」ボタンをクリックする。(Alt-1)
    3. システムは、削除済み地震一覧でチェックされた行の地震IDを元に地震データを復活する。(Alt-2,Alt-3)
    4. システムは、建物IDをパスパラメータに設定し、地震データ復活完了画面を表示する。
    - Alt-1. ユーザーが、確認ダイアログで「いいえ」をクリックした場合
        1. ユースケースを終了する。
    - Alt-2. 選択チェックボックスが全て非チェック状態の場合
        1. システムは、選択チェックボックスが全て非チェック状態のため処理を実施しない旨メッセージを表示する。
        2. ユースケースを終了する。
    - Alt-3. 選択した地震データのいずれかが物理削除されていた場合
        1. システムは、地震データの削除をロールバックする。
        2. システムは、地震データの削除に失敗したことと、操作を再実施する必要がある旨メッセージを表示する。
        3. ユースケースを終了する。
- 地震データ完全削除処理
    1. システムは、確認ダイアログを表示し、地震データを完全削除した場合、
        復元出来ないが処理を継続してよいか確認するメッセージを表示する。
    2. ユーザーは、確認ダイアログで「はい」ボタンをクリックする。(Alt-1)
    3. システムは、削除済み地震一覧でチェックされた行の地震IDを元に削除済み地震データを物理削除する。(Alt-2,Alt-3)
    4. システムは、建物IDをパスパラメータに設定し、地震データ削除完了画面を表示する。
    - Alt-1. ユーザーが、確認ダイアログで「いいえ」をクリックした場合
        1. ユースケースを終了する。
    - Alt-2. 選択チェックボックスが全て非チェック状態の場合
        1. システムは、選択チェックボックスが全て非チェック状態のため処理を実施しない旨メッセージを表示する。
        2. ユースケースを終了する。
    - Alt-3. 選択した地震データのいずれかが復活されていた場合
        1. システムは、地震データの削除をロールバックする。
        2. システムは、地震データの削除に失敗したことと、操作を再実施する必要がある旨メッセージを表示する。
        3. ユースケースを終了する。
- 地震管理画面表示処理
    1. システムは、建物IDをパスパラメータに設定し、削除済み地震一覧画面を表示する。

### 検討事項
NSmosクラウドシステムはVissQを踏襲するが、採用するか否かについて検討する必要がある。
- 検索条件の震度、被災度
    地震管理画面と同様
- ページ送り共通部品
    地震管理画面と同様
- 地震データ完全削除処理
    画面を表示してから削除処理を実施するまでの間に、別のユーザーにより復活されていた場合エラーとなる。
    物理削除されていた場合、処理は正常に実施されるだが、開発の段階で処理が複雑になるようであれば、
    一括でエラーとする。これは他のユーザーとの作業の実施タイミングにより、厳密な管理が出来なくなる可能性を考慮している。
    １秒未満の単位での操作タイミングの重なりを考慮するのであれば、一律再実施にもっていった方が処理がシンプルになるし、
    テストも実施しやすいので。
